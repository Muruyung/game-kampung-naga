using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFruit : MonoBehaviour
{
    [SerializeField]
    private GameObject[] buah;

    private BoxCollider2D col;

    float x1, x2, x3, random;

    void Awake()
    {
        col = GetComponent<BoxCollider2D>();

        x1 = transform.position.x - col.bounds.size.x / 2f;
        x2 = transform.position.x;
        x3 = transform.position.x + col.bounds.size.x / 2f;
    }

    void Start()
    {
        StartCoroutine(Spawn(Random.Range(1f, 2f)));
    }

    IEnumerator Spawn(float time)
    {
        yield return new WaitForSecondsRealtime(time);

        Vector3 temp = transform.position;

        random = Random.Range(x1, x3);
        if (random < -10) temp.x = x1;
        else if (random >= -10 && random <=-6) temp.x = x2;
        else if (random > -6) temp.x = x3;

        Instantiate(buah[Random.Range(0, buah.Length)], temp, Quaternion.identity);

        StartCoroutine(Spawn(Random.Range(1f, 3f)));
    }
}
