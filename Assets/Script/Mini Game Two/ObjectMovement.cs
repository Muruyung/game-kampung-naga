using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectMovement : MonoBehaviour
{
    private Rigidbody2D myBody;
    float time;

    void Awake()
    {
        myBody = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        Vector2 vel = myBody.velocity;
        myBody.velocity = vel;
        time += Time.deltaTime;
        if(time>=5){
            Destroy(gameObject);
        }
    }
    
}
