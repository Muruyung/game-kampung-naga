using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    private Text scoreText;
    private int score = 0;

    void Awake()
    {
        if(PlayerPrefs.GetInt("Test") == 1){
            scoreText = GameObject.Find("Skor").GetComponent<Text>();
            scoreText.text = PlayerPrefs.GetString("Skor");
            PlayerPrefs.SetInt("Test", 0);
        }else{
            scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
            scoreText.text = "0";
        }
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == "Buah")
        {
            Destroy(target.gameObject);
            score+=2;
            scoreText.text = score.ToString();
            PlayerPrefs.SetString("Skor", score.ToString());
            if(score == 100){
                SceneManager.LoadScene("MiniGame2-Result");
            }
            PlayerPrefs.SetInt("Test", 1);
        }
    }


}
