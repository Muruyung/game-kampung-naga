using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour
{
    private Text scoreText;
    private int score = 0;

    void Awake()
    {
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        scoreText.text = "0";
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == "Egg")
        {
            target.gameObject.SetActive(false);
            score++;
            scoreText.text = score.ToString();
            PlayerPrefs.SetInt("scoregame1", score);
        }
    }

}
