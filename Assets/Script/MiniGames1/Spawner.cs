using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] eggs;

    private BoxCollider2D col;

    float x1, x2, x3, random;

    void Awake()
    {
        col = GetComponent<BoxCollider2D>();

        x1 = transform.position.x - col.bounds.size.x / 2f;
        x2 = transform.position.x;
        x3 = transform.position.x + col.bounds.size.x / 2f;
    }

    void Start()
    {
        StartCoroutine(SpawnEgg(1f));
    }

    IEnumerator SpawnEgg(float time)
    {
        yield return new WaitForSecondsRealtime(time);

        Vector3 temp = transform.position;
        //temp.x = Random.Range(x1, x3);
        //temp.x = x1;

        random = Random.Range(x1, x3);
        if (random < -2) temp.x = x1;
        else if (random >= -2 && random <=2) temp.x = x2;
        else if (random > 2) temp.x = x3;

        Debug.Log(random);
        Debug.Log(x1);
        Debug.Log(x2);
        Debug.Log(x3);

        Instantiate(eggs[Random.Range(0, eggs.Length)], temp, Quaternion.identity);

        StartCoroutine(SpawnEgg(Random.Range(1f, 2f)));
    }
}
