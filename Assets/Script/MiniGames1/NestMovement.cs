using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NestMovement : MonoBehaviour
{
    private float speed = 10f;
    private Rigidbody2D myBody;

    void Awake()
    {
        myBody = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        Vector2 vel = myBody.velocity;
        vel.x = Input.GetAxis("Horizontal") * speed;
        myBody.velocity = vel;
    }
}
